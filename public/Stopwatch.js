/**
 * Class representing a simple stopwatch, used to measure the game time.
 */
class Stopwatch {
    /**
     * Creates new stopwatch. Constructor itself does not start measuring (method start() must be called
     * in order to start measuring).
     */
    constructor() {
        this.reset();
    }

    /**
     * Starts the stopwatch. It is not necessary to reset the stopwatch before calling start().
     */
    start() {
        this.starTime = Date.now();
    }

    /**
     *
     * @returns {number} Returns the number of miliseconds from the start of the measurement (method start() invocation).
     */
    getCurrTimeMilis() {
        return Date.now() - this.starTime;
    }

    /**
     *
     * @returns {boolean} Returns true, iff the stopwatch is running, false otherwise.
     */
    isRunning() {
        return this.starTime > 0;
    }

    /**
     * Resets the stopwatch.
     */
    reset() {
        this.starTime = -1;
    }

    /**
     *
     * @returns {string} Returns current time in long (with miliseconds) readable form (mm:ss.sss).
     */
    getCurrTimeReadableLong() {
        let displayMins;
        let displaySecs;
        let displayMilis;
        if (this.isRunning()) {
            const currTimeMilis = this.getCurrTimeMilis();
            const totalSedonds = Math.floor(currTimeMilis / 1000);

            displayMins = Math.floor(totalSedonds / 60).toString();
            displaySecs = (totalSedonds % 60).toString();
            displayMilis = (currTimeMilis % 1000).toString()
        } else {
            displayMins = "0";
            displaySecs = "0";
            displayMilis = "0";
        }
        return `${displayMins.padStart(2, "0")}:${displaySecs.padStart(2, "0")}.${displayMilis.padStart(3, "0")}`

    }

    /**
     *
     * @returns {string} Returns current time in short readable form (mm:ss).
     */
    getCurrTimeReadableShort() {
        let displaySecs = "0";
        let displayMins = "0";

        if (this.isRunning()) {
            const currTimeMilis = this.getCurrTimeMilis();
            const totalSedonds = Math.floor(currTimeMilis / 1000);

            displayMins = Math.floor(totalSedonds / 60).toString();
            displaySecs = (totalSedonds % 60).toString();
        } else {
            displayMins = "0";
            displaySecs = "0";
        }

        return `${displayMins.padStart(2, "0")}:${displaySecs.padStart(2, "0")}`
    }
}