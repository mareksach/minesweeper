(() => {

    const MINE = -1;

    GAME_STATE = {
        RUNNING: 1,
        STOPPED: 2
    };

    const MARK_SOUND = new Audio("resources/audio/mark.mp3");
    const UNCOVER_SOUND = new Audio("resources/audio/uncover.mp3");
    const EXPLOSION_SOUND = new Audio("resources/audio/explosion.mp3");
    const VICTORY_SOUND = new Audio("resources/audio/ta_da_victory.mp3");
    
    MARK_SOUND.load();
    UNCOVER_SOUND.load();
    EXPLOSION_SOUND.load();
    VICTORY_SOUND.load();

    /**
     * Class representing the engine for the "Minesweeper" game. The class can generate the game (place mines,
     * calculate count of adjacent mines to each field and handle uncovering of individual fields) and handle its
     * visualisation.
     */
    class MinesweeperEngine {

        /**
         * Creates new instance of the game.
         *
         * @param width Width of the board.
         * @param height Height of the board.
         * @param mineOccurrenceProbability Probability if mine occurrence on each field.
         */
        constructor(width, height, mineOccurrenceProbability = 0.15) {
            this.width = width;
            this.height = height;
            this.mineOccurrenceProbability = mineOccurrenceProbability;
            this.minesCoords = [];
            this.uncoveredFields = 0;
            this.stopwatch = new Stopwatch();
            this.stopwatchInterval = null;
            this.generateBoard();
            this.gameState = GAME_STATE.RUNNING;
        }

        /**
         * Generates new game board. Places mines randomly and calculates the adjacent mines count of each field.
         * This method is not responsible for the board rendering.
         */
        generateBoard() {
            this.mineBoard = [];
            this.stopwatch.reset();
            this.displayCurrTime();

            let minesCount = 0;

            for (let i = 0; i < this.height; i++) {
                this.mineBoard.push([]);
                for (let j = 0; j < this.width; j++) {
                    const placeMine = Math.random() <= this.mineOccurrenceProbability;
                    this.mineBoard[i].push(new Field(placeMine, this.mineImage, this.flagImage));
                    if (placeMine) {
                        this.minesCoords.push([i, j]);
                    }
                }
            }

            console.log("Mines percentage: ", this.minesCoords.length / (this.width * this.height));

            this.fieldsToBeUncovered = this.width * this.height - this.minesCoords.length;
            this.calculateAdjMinesCounts();

            console.log(this.toString());
        }

        /**
         * Calculates the count of adjacent mines for each field on the board.
         */
        calculateAdjMinesCounts() {
            for (let i = 0; i < this.height; i++) {
                for (let j = 0; j < this.width; j++) {

                    if (!this.mineBoard[i][j].isMine()) {

                        for (let x = i - 1; x <= i + 1; x++) {
                            if (x >= 0 && x < this.height) {
                                for (let y = j - 1; y <= j + 1; y++) {
                                    if (y >= 0 && y < this.width) {
                                        if (!(x == i && y == j)
                                            && this.mineBoard[x][y].isMine()) {
                                            this.mineBoard[i][j].adjMinesCount++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /**
         * Creates html representation of the board, adds event listeners for clicking on the board.
         */
        drawBoard() {
            let boardDiv = document.querySelector("#mineBoard");
            boardDiv.oncontextmenu = (e) => {
                e.preventDefault();
            };

            boardDiv.classList.remove("shake");

            while (boardDiv.firstChild) {
                boardDiv.removeChild(boardDiv.firstChild);
            }

            for (let i = 0; i < this.height; i++) {
                const row = document.createElement("div");
                for (let j = 0; j < this.width; j++) {
                    const field = this.mineBoard[i][j];
                    field.htmlElem.addEventListener("mousedown", (e) => {
                        this.handleClick(e, i, j);
                    });

                    row.appendChild(field.htmlElem);
                }
                boardDiv.appendChild(row)
            }

            const gameResult = document.querySelector("#gameResult");
            gameResult.innerHTML = "";
        }

        /**
         * Plays given sound from the beginning, if sounds are allowed.
         * @param audio Audio object to be played.
         */
        playSoundIfAllowed(audio) {
            const soundsAllowed = document.querySelector("#allowSoundsCheckbox").checked;
            if (soundsAllowed) {
                audio.pause();
                audio.currentTime = 0;
                audio.play();
            }
        }

        /**
         * Handles user's click. Left click uncovers a field, right click places a flag indicating a mine.
         * Click also invokes playing of corresponding audio file.
         *
         * First click of the game also starts the stopwatch.
         *
         * @param e Event representing the click.
         * @param row Row number of the clicked field.
         * @param col Col number of the clicked field.
         */
        handleClick(e, row, col) {
            if (!this.stopwatch.isRunning()) {
                this.stopwatch.start();
                this.stopwatchInterval = setInterval(this.displayCurrTime.bind(this), 1000);
            }

            const field = this.mineBoard[row][col];

            if (this.gameState == GAME_STATE.RUNNING) {
                switch (e.button) {
                    case 0:
                        const mineUncovered = this.uncoverRecursively(row, col);
                        if (mineUncovered) {
                            this.playSoundIfAllowed(EXPLOSION_SOUND);
                            this.endLostGame();
                        } else {
                            if (this.uncoveredFields == this.fieldsToBeUncovered) {
                                this.playSoundIfAllowed(VICTORY_SOUND);
                                this.endWonGame();
                            } else {
                                this.playSoundIfAllowed(UNCOVER_SOUND);
                            }
                        }
                        break;
                    case 2:
                        field.toggleMark();
                        this.playSoundIfAllowed(MARK_SOUND);
                        break;
                }
            }
        }

        /**
         * Displays the current game-time in format mm:ss.
         */
        displayCurrTime() {
            const timeDiv = document.querySelector("#stopwatch");
            timeDiv.innerHTML = this.stopwatch.getCurrTimeReadableShort();
        }

        /**
         * Displays the current game-time in format mm:ss.sss. (mins:secs.milisecs).
         */
        displayCurrTimeLong() {
            const timeDiv = document.querySelector("#stopwatch");
            timeDiv.innerHTML = this.stopwatch.getCurrTimeReadableLong();
        }

        /**
         * Uncovers field specified by its row and column. If the adjacent-mines count of the uncovered field is
         * equal to zero, adjacent fields to the uncovered field are recursively uncovered too.
         *
         * @param row Row to be uncovered
         * @param col Col to be uncovered
         * @returns {boolean} Returns true, if the uncovered field was mine. Returns false otherwise.
         */
        uncoverRecursively(row, col) {

            if (this.mineBoard[row][col].uncovered) {
                return false;
            }

            const mineUncovered = this.mineBoard[row][col].uncover();
            this.uncoveredFields++;

            if (this.mineBoard[row][col].adjMinesCount == 0) {

                for (let x = row - 1; x <= row + 1; x++) {
                    for (let y = col - 1; y <= col + 1; y++) {
                        if (x >= 0 && x < this.height && y >= 0 && y < this.width) {
                            this.uncoverRecursively(x, y);
                        }
                    }
                }
            }

            return mineUncovered;
        }

        /**
         * Ends the game, displays the game-time in long format.
         */
        stopGame() {
            this.gameState = GAME_STATE.STOPPED;
            this.displayCurrTimeLong();
            clearInterval(this.stopwatchInterval);
        }

        /**
         * Stops the game, informs the user about his victory.
         */
        endWonGame() {
            console.log("Game won - all fields without mine uncovered.");
            this.stopGame();
            const gameResult = document.querySelector("#gameResult");
            gameResult.innerHTML = "You won!";
        }

        /**
         * Stops the game, informs the user about his loss.
         */
        endLostGame() {
            console.log("Game lost - mine uncovered.");
            this.stopGame();
            this.uncoverMines();
            const gameResult = document.querySelector("#gameResult");
            gameResult.innerHTML = "You lost!";
            const mineBoard = document.querySelector("#mineBoard");
            mineBoard.classList.add("shake");
        }

        /**
         * Uncovers all mines on the board. This is primarily meant to be done after a user uncovers a mine and loses
         * the game.
         */
        uncoverMines() {
            for (let i = 0; i < this.height; i++) {
                for (let j = 0; j < this.width; j++) {
                    const field = this.mineBoard[i][j];
                    if (field.isMine() && !field.uncovered && !field.marked) {
                        field.uncover();
                    } else if (field.marked && !field.isMine()) {
                        field.setAsWronglyMarked();
                    }
                }
            }
        }

        /**
         *
         * @returns {string|string} String representation of the board. Each field is represented by the number
         * of its adjacent mines, a mine is represented by -1.
         */
        toString() {
            let ret = "";
            for (let i = 0; i < this.height; i++) {
                ret += `${this.mineBoard[i].toString()}\n`;
            }
            return ret;
        }
    }


    /**
     * Class representing a single mine field and handling its html representation. The class provides method
     * for handling of uncovering or marking particular field. In case of such event, it also adds corresponding
     * html attributes to its html element, so it is rendered properly in the web browser.
     */
    class Field {

        /**
         * Constructs new instance of a field.
         * @param isMine
         */
        constructor(isMine = false) {
            this.adjMinesCount = isMine ? MINE : 0;
            this.htmlElem = document.createElement("div");
            this.htmlElem.setAttribute("adjMinesCount", this.adjMinesCount);
            this.uncovered = false;
            this.marked = false;

            if (this.isMine()) {
                this.htmlElem.classList.add("mine");
            }
        }

        isMine() {
            return this.adjMinesCount == MINE;
        }

        /**
         *
         * @returns {boolean} Returns true, if the uncovered field is mine. Returns else otherwise.
         */
        uncover() {
            this.htmlElem.classList.add("uncovered");
            if (this.adjMinesCount > 0) {
                this.htmlElem.textContent = this.adjMinesCount;
            }
            this.uncovered = true;
            this.htmlElem.classList.remove("marked");

            return this.isMine();
        }

        /**
         * Adds mark (flag) to a field, iff the field is not marked.
         * Removes mark (flag) from a field, iff the field is not marked.
         */
        toggleMark() {
            if (!this.uncovered) {
                this.htmlElem.classList.toggle("marked");
                this.marked = !this.marked;
            }
        }

        /**
         * Sets the field as wrongly marked (field was marked, even though there was no mine).
         * Assigns corresponding html attributes to its html element, so it is rendered correctly.
         */
        setAsWronglyMarked() {
            this.htmlElem.classList.remove("marked");
            this.htmlElem.classList.add("wrongly-marked");
        }

        toString() {
            return this.isMine() ? '*' : this.adjMinesCount.toString()
        }
    }

    let engine = new MinesweeperEngine(0, 0);

    const newGameBtn = document.querySelector("#newGameBtn");
    newGameBtn.addEventListener("click", () => {
            initNewGame();
        }
    );


    /**
     * Functions creates new game. The board size is based on the seleced difficulty.
     */
    function initNewGame() {
        const difficulty = document.querySelector("#difficultySettings input:checked").value;

        if (engine.gameState == GAME_STATE.RUNNING) {
            engine.stopGame();
        }

        switch (difficulty) {
            case "easy":
                engine = new MinesweeperEngine(10, 10);
                break;
            case "medium":
                engine = new MinesweeperEngine(16, 16);
                break;
            case "hard":
                engine = new MinesweeperEngine(24, 24);
                break;
        }

        engine.drawBoard();
    }

    const difficulty = document.querySelectorAll("#difficultySettings input");
    for (const radio of difficulty) {
        radio.addEventListener('click', initNewGame);
    }

    initNewGame();

})();



