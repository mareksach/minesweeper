![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

# Minesweeper

This is a single page implementation of the minesweeper game.

The player can select from 3 difficulties (easy, medium, hard).

Sound effects can be enabled/disabled by user. In addition, if mine
is accidentally uncovered, the whole field shakes little bit.

## Implementation
The game uses plain html, JavaScript and CSS, no additional library has been used.